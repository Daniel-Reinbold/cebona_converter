import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.*;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class Main {

    public static void main(String[] args) throws IOException {

        /*0*/	String plstnr = ""; /*1*/	String station = ""; /*2*/	String kostenstelle = ""; /*3*/	String persnr = ""; /*4*/	String name = ""; /*5*/	String symbol = ""; /*6*/	String day_01 = ""; /*7*/	String day_02 = ""; /*8*/	String day_03 = ""; /*9*/	String day_04 = ""; /*10*/	String day_05 = ""; /*11*/	String day_06 = ""; /*12*/	String day_07 = ""; /*13*/	String day_08 = ""; /*14*/	String day_09 = ""; /*15*/	String day_10 = ""; /*16*/	String day_11 = ""; /*17*/	String day_12 = ""; /*18*/	String day_13 = ""; /*19*/	String day_14 = ""; /*20*/	String day_15 = ""; /*21*/	String day_16 = ""; /*22*/	String day_17 = ""; /*23*/	String day_18 = ""; /*24*/	String day_19 = ""; /*25*/	String day_20 = ""; /*26*/	String day_21 = ""; /*27*/	String day_22 = ""; /*28*/	String day_23 = ""; /*29*/	String day_24 = ""; /*30*/	String day_25 = ""; /*31*/	String day_26 = ""; /*32*/	String day_27 = ""; /*33*/	String day_28 = ""; /*34*/	String day_29 = ""; /*35*/	String day_30 = ""; /*36*/	String day_31 = ""; /*37*/	String total = ""; /*38*/	String von = ""; /*39*/	String bis = "";

        //Definition der Zeitangabe für Ausgabe im Namen
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd_HH-mm-ss");
        Date currentTime = new Date();
        String datum = formatter.format(currentTime);

        //Konsolen-Programm-Auswahl

        java.util.Scanner scanner = new java.util.Scanner(System.in);
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("---------------- Auswahl -----------------------------------------------------");
        System.out.println("---- 1. In-Output: Simples Einlesen von einer CSV-Datei ----------------------");
        System.out.println("---- 2. Langer Text mit automatischem Seitenumbruch --------------------------");
        System.out.println("---- 3. Create Table-1: Normale kleine Tabelle  ------------------------------");
        System.out.println("---- 4. Create Table-2: Tabelle über mehrere Zeilen --------------------------");
        System.out.println("------------------------------------------------------------------------------");
        int auswahlSc = scanner.nextInt();

        switch (auswahlSc){
            case 1:
                String csvFileIn = "\\Users\\drein\\Desktop\\IdeaProjects\\OUTPUT\\Cebona Prüfliste\\Beispieldaten\\test2.csv";
                convert01(csvFileIn);
                break;
            case 2:
                String text = "Hans hatte sieben Jahre bei seinem Herrn gedient, da sprach er zu ihm 'Herr, meine Zeit ist herum, nun wollte ich gerne wieder heim zu meiner Mutter, gebt mir meinen Lohn.' Der Herr antwortete 'du hast mir treu und ehrlich gedient, wie der Dienst war, so soll der Lohn sein,' und gab ihm ein Stück Gold, das so groß als Hansens Kopf war. Hans zog sein Tüchlein aus der Tasche, wickelte den Klumpen hinein, setzte ihn auf die Schulter und machte sich auf den Weg nach Haus. Wie er so dahin gieng und immer ein Bein vor das andere setzte, kam ihm ein Reiter in die Augen, der frisch und fröhlich auf einem muntern Pferd vorbei trabte. 'Ach,' sprach Hans ganz laut, 'was ist das Reiten ein schönes Ding! da sitzt einer wie auf einem Stuhl, stößt sich an keinen Stein, spart die Schuh, und kommt fort, er weiß nicht wie.' Der Reiter, der das gehört hatte, hielt an und rief 'ei, Hans, warum laufst du auch zu Fuß?' 'Ich muß ja wohl,' antwortete er, 'da habe ich einen Klumpen heim zu tragen: es ist zwar Gold, aber ich kann den Kopf dabei nicht gerad halten, auch drückt mirs auf die Schulter.' 'Weißt du was,' sagte der Reiter, 'wir wollen tauschen: ich gebe dir mein Pferd, und du gibst mir deinen Klumpen.' 'Von Herzen gern,' sprach Hans, 'aber ich sage euch ihr müßt euch damit schleppen.' Der Reiter stieg ab, nahm das Gold und half dem Hans hinauf, gab ihm die Zügel fest in die Hände und sprach 'wenns nun recht geschwind soll gehen, so mußt du mit der Zunge schnalzen, und hopp hopp rufen.'Hans war seelenfroh, als er auf dem Pferde saß und so frank und frei dahin ritt. Über ein Weilchen fiels ihm ein, es sollte noch schneller gehen, und fieng an mit der Zunge zu schnalzen und hopp hopp zu rufen. Das Pferd setzte sich in starken Trab, und ehe sichs Hans versah, war er abgeworfen und lag in einem Graben, der die Äcker von der Landstraße trennte. Das Pferd wäre auch durchgegangen, wenn es nicht ein Bauer aufgehalten hätte, der des Weges kam und eine Kuh vor sich her trieb. Hans suchte seine Glieder zusammen und machte sich wieder auf die Beine. Er war aber verdrießlich und sprach zu dem Bauer 'es ist ein schlechter Spaß, das Reiten, zumal, wenn man auf so eine Mähre geräth wie diese, die stößt und einen herabwirft, daß man den Hals brechen kann; ich setze mich nun und nimmermehr wieder auf. Da lob ich mir eure Kuh, da kann einer mit Gemächlichkeit hinter her gehen und hat obendrein seine Milch, Butter und Käse jeden Tag gewiß. Was gäb ich darum, wenn ich so eine Kuh hätte!' ' Nun,' sprach der Bauer, 'geschieht euch so ein großer Gefallen, so will ich euch wohl die Kuh für das Pferd vertauschen.' Hans willigte mit tausend Freuden ein: der Bauer schwang sich aufs Pferd und ritt eilig davon.Hans trieb seine Kuh ruhig vor sich her und bedachte den glücklichen Handel. 'Hab ich nur ein Stück Brot, und daran wird mirs doch nicht fehlen, so kann ich, so oft mirs beliebt, Butter und Käse dazu essen; hab ich Durst, so melk ich meine Kuh und trinke Milch. Herz, was verlangst du mehr?' Als er zu einem Wirthshaus kam, machte er Halt, aß in der großen Freude alles, was er bei sich hatte, sein Mittags- und Abendbrot, rein auf, und ließ sich für seine letzten paar Heller ein halbes Glas Bier einschenken. Dann trieb er seine Kuh weiter, immer nach dem Dorfe seiner Mutter zu. Die Hitze ward drückender, je näher der Mittag kam, und Hans befand sich in einer Heide, die wohl noch eine Stunde dauerte. Da ward es ihm ganz heiß, so daß ihm vor Durst die Zunge am Gaumen klebte. 'Dem Ding ist zu helfen,' dachte Hans, 'jetzt will ich meine Kuh melken und mich an der Milch laben.' Er band, sie an einen dürren Baum, und da er keinen Eimer hatte, so stellte er seine Ledermütze unter, aber wie er sich auch bemühte, es kam kein Tropfen Milch zum Vorschein. Und weil er sich ungeschickt dabei anstellte, so gab ihm das ungeduldige Thier endlich mit einem der Hinterfüße einen solchen Schlag vor den Kopf, daß er zu Boden taumelte und eine zeitlang sich gar nicht besinnen konnte wo er war. Glücklicherweise kam gerade ein Metzger des Weges, der auf einem Schubkarren ein junges Schwein hegen hatte. 'Was sind das für Streiche!' rief er und half dem guten Hans auf. Hans erzählte was vorgefallen war. Der Metzger reichte ihm seine Flasche und sprach 'da trinkt einmal und erholt euch. Die Kuh will wohl keine Milch geben, das ist ein altes Thier, das höchstens noch zum Ziehen taugt oder zum Schlachten.' 'Ei, ei,' sprach Hans, und strich sich die Haare über den Kopf, 'wer hätte das gedacht! es ist freilich gut, wenn man so ein Thier ins Haus abschlachten kann, was gibts für Fleisch! aber ich mache mir aus dem Kuhfleisch nicht viel, es ist mir nicht saftig genug. Ja, wer so ein junges Schwein hätte! das schmeckt anders, dabei noch die Würste.' 'Hört, Hans,' sprach da der Metzger, 'euch zu Liebe will ich tauschen und will euch das Schwein für die Kuh lassen.' 'Gott lohn euch eure Freundschaft' sprach Hans, übergab ihm die Kuh, ließ sich das Schweinchen vom Karren losmachen und den Strick, woran es gebunden war, in die Hand geben.Hans zog weiter und überdachte wie ihm doch alles nach Wunsch gienge, begegnete ihm ja eine Verdrießlichkeit, so würde sie doch gleich wieder gut gemacht. Es gesellte sich danach ein Bursch zu ihm, der trug eine schöne weiße Gans unter dem Arm. Sie boten einander die Zeit, und Hans fieng an von seinem Glück zu erzählen und wie er immer so vortheilhaft getauscht hätte. Der Bursch erzählte ihm daß er die Gans zu einem Kindtaufschmaus brächte. 'Hebt einmal,' fuhr er fort, und packte sie bei den Flügeln, 'wie schwer sie ist, die ist aber auch acht Wochen lang genudelt worden. Wer in den Braten beißt, muß sich das Fett von beiden Seiten abwischen.' 'Ja,' sprach Hans, und wog sie mit der einen Hand, 'die hat ihr Gewicht, aber mein Schwein ist auch keine Sau.' Indessen sah sich der Bursch nach allen Seiten ganz bedenklich um, schüttelte auch wohl mit dem Kopf. 'Hört,' fieng er darauf an, 'mit eurem Schweine mags nicht ganz richtig sein. In dem Dorfe, durch das ich gekommen bin, ist eben dem Schulzen eins aus dem Stall gestohlen worden. Ich fürchte, ich fürchte, ihr habts da in der Hand. Sie haben Leute ausgeschickt, und es wäre ein schlimmer Handel, wenn sie euch mit dem Schwein erwischten: das geringste ist, daß ihr ins finstere Loch gesteckt werdet.' Dem guten Hans ward bang, 'ach Gott,' sprach er, 'helft mir aus der Noth, ihr wißt hier herum bessern Bescheid, nehmt mein Schwein da und laßt mir eure Gans.' 'Ich muß schon etwas aufs Spiel setzen,' antwortete der Bursche, 'aber ich will doch nicht Schuld sein daß ihr ins Unglück gerathet.' Er nahm also das Seil in die Hand und trieb das Schwein schnell auf einen Seitenweg fort: der gute Hans aber ging, seiner Sorgen entledigt, mit der Gans unter dem Arme der Heimath zu. 'Wenn ichs recht überlege,' sprach er mit sich selbst, 'habe ich noch Vortheil bei dem Tausch: erstlich den guten Braten, hernach die Menge von Fett, die herausträufeln wird, das gibt Gänsefettbrot auf ein Vierteljahr: und endlich die schönen weißen Federn, die laß ich mir in mein Kopfkissen stopfen, und darauf will ich wohl ungewiegt einschlafen. Was wird meine Mutter eine Freude haben!' ls er durch das letzte Dorf gekommen war, stand da ein Scherenschleifer mit seinem Karren, sein Rad schnurrte, und er sang dazu ich schleife die Scheere, und drehe geschwind, und hänge mein Mäntelchen nach dem Wind.' Hans blieb stehen und sah ihm zu; endlich redete er ihn an, und sprach 'euch gehts wohl, weil ihr so lustig bei eurem Schleifen, seid.' 'Ja,' antwortete der Scherenschleifer, 'das Handwerk hat einen güldenen Boden. Ein rechter Schleifer ist ein Mann, der, so oft er in die Tasche greift, auch Geld darin findet. Aber wo habt ihr die schöne Gans gekauft?' 'Die hab ich nicht gekauft, sondern für mein Schwein eingetauscht.' 'Und das Schwein?' 'Das hab ich für eine Kuh gekriegt.' 'Und die Kuh?' 'Die hab ich für ein Pferd bekommen,' 'Und das Pferd?' 'Dafür hab ich einen Klumpen Gold, so groß als mein Kopf, gegeben.' 'Und das Gold?' 'Ei, das war mein Lohn für sieben Jahre Dienst.' 'Ihr habt euch jederzeit zu helfen gewußt,' sprach der Schleifer, 'könnt ihrs nun dahin bringen, daß ihr das Geld in der Tasche springen hört, wenn ihr aussteht, so habt ihr euer Glück gemacht.' 'Wie soll ich das anfangen?' sprach Hans. 'Ihr müßt ein Schleifer werden, wie ich; dazu gehört eigentlich nichts, als ein Wetzstein, das andere findet sich schon von selbst. Da hab ich einen, der ist zwar ein wenig schadhaft, dafür sollt ihr mir aber auch weiter nichts als eure Gans geben; wollt ihr das?' 'Wie könnt ihr noch fragen,' antwortete Hans, 'ich werde ja zum glücklichsten Menschen auf Erden; habe ich Geld, so oft ich in die Tasche greife, was brauche ich da länger zu sorgen?' reichte ihm die Gans hin, und nahm den Wetzstein in Empfang. 'Nun,' sprach der Schleifer, und hob einen gewöhnlichen schweren Feldstein, der neben ihm lag, auf, 'da habt ihr noch einen tüchtigen Stein dazu, auf dem sichs gut schlagen läßt, und ihr eure alten Nägel gerade klopfen könnt. Nehmt ihn und hebt ihn ordentlich auf.' Hans lud den Stein auf und ging mit vergnügtem Herzen weiter; seine Augen leuchteten vor Freude, 'ich muß in einer Glückshaut geboren sein,' rief er aus, 'alles was ich wünsche trifft mir ein, wie einem Sonntagskind.' Indessen, weil er seit Tagesanbruch auf den Beinen gewesen war, begann er müde zu werden; auch plagte ihn der Hunger, da er allen Vorrat, auf einmal in der Freude über die erhandelte Kuh aufgezehrt hatte. Er konnte endlich nur mit Mühe weiter gehen und mußte jeden Augenblick Halt machen; dabei drückten ihn die Steine ganz erbärmlich. Da konnte er sich des Gedankens nicht erwehren, wie gut es wäre, wenn er sie gerade jetzt nicht zu tragen brauchte. Wie eine Schnecke kam er zu einem Feldbrunnen geschlichen, wollte da ruhen und sich mit einem frischen Trunk laben: damit er aber die Steine im Niedersitzen nicht beschädigte, legte er sie bedächtig neben sich auf den Rand des Brunnens. Darauf setzte er sich nieder und wollte sich zum Trinken bücken, da versah ers, stieß ein klein wenig an, und beide Steine plumpten hinab. Hans, als er sie mit seinen Augen in die Tiefe hatte versinken sehen, sprang vor Freuden auf, kniete dann nieder und dankte Gott mit Thränen in den Augen daß er ihm auch diese Gnade noch erwiesen und ihn auf eine so gute Art und ohne daß er sich einen Vorwurf zu machen brauchte, von den schweren Steinen befreit hätte, die ihm allein noch hinderlich gewesen wären. 'So glücklich wie ich,' rief er aus, 'gibt es keinen Menschen unter der Sonne.' Mit leichtem Herzen und frei von aller Last sprang er nun fort, bis er daheim bei seiner Mutter war. Jacob Grimm (1785-1863) und Wilhelm Grimm (1786-1859)";
                //CreatePDF001.createHelloDocument();
                CreatePDF001.c2(text);
                break;
            case 3:
                PDDocument doc = new PDDocument();
                PDPage page = new PDPage();
                doc.addPage( page );

                PDPageContentStream contentStream =
                        new PDPageContentStream(doc, page);

                String[][] content = {
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"}
                } ;

                CreatePDF001.drawTable(page, contentStream, 700, 100, content);
                contentStream.close();
                doc.save("OUTPUT\\Table-1_" + datum + ".pdf" );
                break;
            case 4:
                PDDocument doca = new PDDocument();
                PDPage pagea = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
                doca.addPage( pagea );

                PDPageContentStream contentStreama =
                        new PDPageContentStream(doca, pagea);

                String[][] contenta = {
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5", "a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000"},
                        {"a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000", "a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"},
                        {"a","b", "1", "d", "e", "100000"},
                        {"c","d", "2"},
                        {"e","f", "3"},
                        {"g","h", "4", "Daniel"},
                        {"i","j", "5"}

                } ;

                CreatePDF001.drawTable(pagea, contentStreama, 700, 100, contenta);
                contentStreama.close();
                doca.save("OUTPUT\\Table-2_" + datum + ".pdf" );
                break;
            default:
                System.out.println("Falsche Eingabe!");
        }
    }

    public static void convert01(String csvFile){

        /*
        * TODO Simple In-/Output
        * */

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] element = line.split(cvsSplitBy);

                String vglplstnr = "";
                vglplstnr = element[0];
                if(vglplstnr == element[0]){
                    System.out.println(" [ " + vglplstnr + " ] ");
                }else{

                }

                for(int i=0; i< element.length; i++) {
                        System.out.print(element[i] + "\t");
                }
                System.out.println("");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return;
    }

    public static void convert02(String csvFile){

        /*
        * TODO Mehrdimensionales Array - Funktioniert noch nicht!
        * */

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator

                String[][] element = new String[100][40];


                for(int i=0; i< element.length; i++) {
                    for(int j=0; j<i; j++) {
                        System.out.print(element[i][j]);
                    }
                }
                System.out.println("");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return;
    }

}

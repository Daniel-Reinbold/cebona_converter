import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.*;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class CreatePDF001 {



    public static void createHelloDocument() throws IOException {
        final PDPage firstPage = new PDPage();
        final PDFont courierBoldFont = PDType1Font.COURIER_BOLD;
        final PDFont Helvetiva = PDType1Font.HELVETICA;
        final int fontSize = 10;
        try (final PDDocument document = new PDDocument())
        {
            document.addPage(firstPage);
            final PDPageContentStream contentStream = new PDPageContentStream(document, firstPage);

            contentStream.beginText();
            contentStream.setFont(Helvetiva, fontSize);

            contentStream.newLineAtOffset(25, 750); //tx = Abstand von links / ty= Abstand von unten --> Koordinatensystem
            contentStream.showText("______________________________________");
            contentStream.newLine();

            int TY = 750;

            for(int i=0; i< 100; i++){

                contentStream.showText("Test: " + i );
            }


            contentStream.endText();
            contentStream.close();  // Stream must be closed before saving document.
            document.save("HelloPDFBox.pdf");
        }
        catch (IOException ioEx)
        {
            System.err.println(
                    "Es ist ein Fehler bei der Erstellung des Dokumentes aufgetreten - " + ioEx);
        }
    }

    public static void c2(String text) throws IOException{


        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd_HH-mm-ss");
        Date currentTime = new Date();
        System.out.println(formatter.format(currentTime));
        String datum = formatter.format(currentTime);


        PDDocument doc = null;
        try
        {
            doc = new PDDocument();
            //PDPage page = new PDPage();
            PDPage page = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
            PDPage page2 = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
            doc.addPage(page);
            doc.addPage(page2);

            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            PDFont pdfFont = PDType1Font.HELVETICA;
            float fontSize = 10;
            float leading = 1.5f * fontSize;

            PDRectangle mediabox = page.getMediaBox();
            float margin = 20;
            float width = mediabox.getWidth() - 2*margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;



            List<String> lines = new ArrayList<String>();
            int lastSpace = -1;
            while (text.length() > 0)
            {
                int spaceIndex = text.indexOf(' ', lastSpace + 1);
                if (spaceIndex < 0)
                    spaceIndex = text.length();
                String subString = text.substring(0, spaceIndex);
                float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
                System.out.printf("'%s' - %f of %f\n", subString, size, width);
                if (size > width)
                {
                    if (lastSpace < 0)
                        lastSpace = spaceIndex;
                    subString = text.substring(0, lastSpace);
                    lines.add(subString);
                    text = text.substring(lastSpace).trim();
                    System.out.printf("'%s' is line\n", subString);
                    lastSpace = -1;
                }
                else if (spaceIndex == text.length())
                {
                    lines.add(text);
                    System.out.printf("'%s' is line\n", text);
                    text = "";
                }
                else
                {
                    lastSpace = spaceIndex;
                }
            }

            /*
            * Todo - Neue Seite
            * Seitenumbruch wenn es zu viele Lines für eine Seite gibt.
            * Eine Neue Seite erstellen.
            * Hier mit dem Text fortfahren.,
            * */



            contentStream.beginText();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.newLineAtOffset(startX, startY);
            for (String line: lines)
            {
                contentStream.showText(line);
                contentStream.newLineAtOffset(0, -leading);
            }
            contentStream.endText();
            contentStream.close();

            doc.save(new File("OUTPUT\\OUT_" + datum + ".pdf"));
        }
        finally
        {
            if (doc != null)
            {
                doc.close();
            }
        }
    }

    public static void drawTable(PDPage page, PDPageContentStream contentStream,
                                 float y, float margin,
                                 String[][] content) throws IOException {
        final int rows = content.length;
        final int cols = content[0].length;
        final float rowHeight = 20f;
        final float tableWidth = page.getMediaBox().getWidth()-(2*margin);
        final float tableHeight = rowHeight * rows;
        final float colWidth = tableWidth/(float)cols;
        final float cellMargin=5f;

        //draw the rows
        float nexty = y ;
        for (int i = 0; i <= rows; i++) {
            contentStream.drawLine(margin,nexty,margin+tableWidth,nexty);
            nexty-= rowHeight;
        }

        //draw the columns
        float nextx = margin;
        for (int i = 0; i <= cols; i++) {
            contentStream.drawLine(nextx,y,nextx,y-tableHeight);
            nextx += colWidth;
        }

        //now add the text
        contentStream.setFont(PDType1Font.HELVETICA_BOLD,12);

        float textx = margin+cellMargin;
        float texty = y-15;
        for(int i = 0; i < content.length; i++){
            for(int j = 0 ; j < content[i].length; j++){
                String text = content[i][j];
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;
            }
            texty-=rowHeight;
            textx = margin+cellMargin;
        }
    }

}
